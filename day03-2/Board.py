from PathSegment import PathSegment
from Point import Point


class Board:
    EMPTY_SQUARE = ' '
    INTERSECTION_SQUARE = 'X'

    def __init__(self, max_side : int):
        self._max_side = max_side
        self._origin = Point(int(self._max_side/2), int(self._max_side/2))
        self._square = [[self.EMPTY_SQUARE for x in range(self._max_side)] for y in range(self._max_side)]
        self._square[self._origin.x()][self._origin.y()] = 'O'
        self._intersections = []

    def draw_path(self, path_segments: [PathSegment], mark_intersections: bool):
        pointer = self._origin.copy()
        for segment in path_segments:
            pointer = self.draw_segment(segment, pointer, mark_intersections)

    def draw_segment(self, segment: PathSegment, origin: Point, mark_intersections: bool) -> Point:
        pointer = origin.copy()
        for step in range(segment.distance()):
            pointer = self.draw_square(segment, pointer, mark_intersections)
        return pointer

    def draw_square(self, segment: PathSegment, pointer: Point, mark_intersections: bool) -> Point:
        # print("draw_square(pointer={})".format(pointer))
        pointer.move(segment.direction())
        # print("draw_square(moved pointer={})".format(pointer))
        print('.', end='')
        if (self._square[pointer.x()][pointer.y()] == self.EMPTY_SQUARE or not mark_intersections):
            self._square[pointer.x()][pointer.y()] = segment.direction()
        else:
            self._intersections.append(pointer.copy())
            self._square[pointer.x()][pointer.y()] = self.INTERSECTION_SQUARE
        return pointer

    def __str__(self) -> str:
        return self.__repl__()

    def __repl__(self) -> str:
        output = ''
        for y in reversed(range(self._max_side)):
            for x in range(self._max_side):
                char = self._square[x][y]
                if (char == self.EMPTY_SQUARE):
                    output += '.'
                else:
                    output += char
            output += "\n"
        return output

    def distance_to_nearest_intersection(self) -> int:
        """
        Find intersections between paths on each
        board, and return the distance to the 
        origin from the closest intersection.
        """
        closest_distance = 0
        closest_point = None
        for intersection_point in self._intersections:
            distance_to_origin = intersection_point.distance(self._origin)
            print("Found intersection at {intersection_point} at distance ({distance_to_origin}) from {self._origin}".format(**locals()))
            if (closest_point is None):
                closest_point = intersection_point
                closest_distance = distance_to_origin
            elif (distance_to_origin < closest_distance):
                closest_point = intersection_point
                closest_distance = distance_to_origin
        return closest_distance
