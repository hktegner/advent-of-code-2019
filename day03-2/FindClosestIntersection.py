import os
from Circuit import Circuit
from Point import Point
from SegmentDescriptor import SegmentDescriptor

def find_min_signal_delay(path1: str, path2: str) -> int:
    """
    Find the minimum signal delay from any intersection between
    path1 and path2, i.e. the minimum combined distance of the
    paths up to each intersection point.
    """

    circuit1_descriptors = [SegmentDescriptor(desc) for desc in path1.split(",")]
    circuit2_descriptors = [SegmentDescriptor(desc) for desc in path2.split(",")]

    origin = Point(0, 0)
    circuit1 = Circuit("c1", origin, circuit1_descriptors)
    circuit2 = Circuit("c2", origin, circuit2_descriptors)

    print("{}".format(circuit1))
    print("{}".format(circuit2))

    intersections = Circuit.find_intersections(circuit1, circuit2)
    intersections = list(filter(lambda p: not p == origin, intersections))

    # If no intersections found, return an invalid distance to signal that.
    if (len(intersections) == 0):
        print("Found no intersections")
        return -1

    print("Found intersections => {}".format(intersections))

    signal_delays = list(map(lambda p: circuit1.distance_to_intersection(p) + circuit2.distance_to_intersection(p), intersections))

    print("Signal delays => {}".format(signal_delays))

    min_signal_delay = min(signal_delays)

    print("Minimum signal delay = {}".format(min_signal_delay))

    return min_signal_delay


def find_nearest_intersection(path1: str, path2: str) -> int:
    """
    Find nearest intersection relative to the origin between path1 and path2.
    Paths are given as a set of segment descriptors assumed to both start at
    the origin. The distance from an intersection is calculated as the 
    Manhattan distance, i.e. the distance along vertical or horizontal steps.
    """
    
    circuit1_descriptors = [SegmentDescriptor(desc) for desc in path1.split(",")]
    circuit2_descriptors = [SegmentDescriptor(desc) for desc in path2.split(",")]

    origin = Point(0, 0)
    circuit1 = Circuit("c1", origin, circuit1_descriptors)
    circuit2 = Circuit("c2", origin, circuit2_descriptors)

    print("{}".format(circuit1))
    print("{}".format(circuit2))

    intersections = Circuit.find_intersections(circuit1, circuit2)
    if (len(intersections) > 0 and intersections[0].x() == 0 and intersections[0].y() == 0):
        intersections = intersections[1:] # ignore origin
    distances = list(map(lambda p: p.manhattan_distance() , intersections))

    print("Found intersections => {}".format(intersections))

    # If no intersections found, return an invalid distance to signal that.
    if (len(distances) == 0):
        return -1

    min_distance = min(distances)

    print("Distance to nearest intersection = {}".format(min_distance))

    return min_distance
