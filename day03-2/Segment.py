from typing import Union
from Point import Point
from SegmentDescriptor import SegmentDescriptor

class Segment:

    def __init__(self, start: Point, descriptor: SegmentDescriptor):
        self._descriptor = descriptor
        self._start = start.copy()
        self._end = start.copy()
        self._end.move(descriptor)

    def start(self):
        return self._start

    def end(self):
        return self._end

    def __str__(self) -> str:
        return '{}-({})-to-({})'.format(self._descriptor, self._start, self._end)

    def __repr__(self) -> str:
        return self.__str__()

    def horizontal(self) -> bool:
        return self._descriptor.direction() in ['L', 'R']

    def vertical(self) -> bool:
        return self._descriptor.direction() in ['U', 'D']

    def length(self) -> int:
        return self._descriptor.distance()

    def distance_from_start(self, intersection: Point) -> int:
        '''
        Returns -1 if no intersection.
        Otherwise the distance from the start of the line to the given point.
        '''
        direction = self._descriptor.direction()
        descriptor = SegmentDescriptor("{dir}{dist}".format(dir=direction, dist=1))
        test_point = self._start
        for distance in range(1, self._descriptor.distance()+1):
            test_point.move(descriptor)
            if test_point == intersection:
                return distance
        return -1

    def contains(self, point: Point) -> bool:
        if self.horizontal():
            min_x = min(self._start.x(), self._end.x())
            max_x = max(self._start.x(), self._end.x())
            return point.y() == self._start.y() and (min_x <= point.x() <= max_x)
        else: # vertical
            min_y = min(self._start.y(), self._end.y())
            max_y = max(self._start.y(), self._end.y())
            return point.x() == self._start.x() and (min_y <= point.y() <= max_y)

    def intersection(self, other: 'Segment') -> Union[None,Point]:
        if (self.horizontal() and other.horizontal()):
            return None
        elif (self.vertical() and other.vertical()):
            return None

        if (self.horizontal()):
            horizontal = self
            vertical = other
        else:
            horizontal = other
            vertical = self

        # print("horizontal={}, vertical={}".format(horizontal, vertical))

        x = vertical.start().x()
        y = horizontal.start().y()

        min_x = min(horizontal.start().x(), horizontal.end().x())
        max_x = max(horizontal.start().x(), horizontal.end().x())
        min_y = min(vertical.start().y(), vertical.end().y())
        max_y = max(vertical.start().y(), vertical.end().y())
        
        if (min_x <= x <= max_x and min_y <= y <= max_y):
            return Point(x, y)
        else:
            return None