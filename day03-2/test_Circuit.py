import pytest

from Segment import Segment
from SegmentDescriptor import SegmentDescriptor
from Point import Point
from Circuit import Circuit

@pytest.mark.parametrize(
"string_descriptors,intersection,expected_distance",
[
    (["R10", "U5"], Point(10,3), 13),
    (["U5", "R10", "U4", "L3"], Point(8,9), 21),
])
def test_distance_to_intersection(string_descriptors: [str], intersection: Point, expected_distance: int):
    descriptors = [SegmentDescriptor(desc) for desc in string_descriptors]
    circuit = Circuit("c1", Point(0,0), descriptors)

    assert circuit.distance_to_intersection(intersection) == expected_distance
