

def execute_program(program: [int]):
    current = 0
    while True:
        opscode = program[current]
        if opscode == 99:
            break
        elif opscode == 1:
            program[program[current+3]] = program[program[current+1]] + program[program[current+2]]
        elif opscode == 2:
            program[program[current+3]] = program[program[current+1]] * program[program[current+2]]
        else:
            raise Exception("Invalid opscode")
        current += 4
    return program


test_data = [
    dict(program=[1,0,0,0,99], result=[2,0,0,0,99]),
    dict(program=[2,3,0,3,99], result=[2,3,0,6,99]),
    dict(program=[2,4,4,5,99,0], result=[2,4,4,5,99,9801]),
    dict(program=[1,1,1,4,99,5,6,0,99], result=[30,1,1,4,2,5,6,0,99])
]

for test in test_data:
    program = test['program']
    expected_result = test['result']
    actual_result = execute_program(program)
    print("Test: program={program}, expected result={expected_result}, actual result={actual_result}".format(**locals()))

# Read intcode program
fh = open("1202.txt", "r")
original_program = list(int(x) for x in fh.read().split(","))
print("Program = {}".format(program))
fh.close()

# Iterate to find solution
max_iterations = 100
required_solution = 19690720
for (noun, verb) in ((noun, verb) for noun in range(0, max_iterations) for verb in range(0, max_iterations)):
    program = original_program.copy()
    program[1] = noun
    program[2] = verb
    print(".", end="")
    end_state = execute_program(program)
    result = end_state[0]
    if (result == required_solution):
        print("Found solution - noun={}, verb={}".format(noun, verb))
        break
