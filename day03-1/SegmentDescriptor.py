
class SegmentDescriptor:

    def __init__(self, descriptor: str):
        self._direction = descriptor[0]
        self._distance = int(descriptor[1:])

    def is_up(self) -> bool:
        return self._direction == 'U'

    def is_down(self) -> bool:
        return self._direction == 'D'

    def is_left(self) -> bool:
        return self._direction == 'L'

    def is_right(self) -> bool:
        return self._direction == 'R'

    def direction(self) -> str:
        return self._direction

    def distance(self) -> int:
        return self._distance
    
    def __str__(self) -> str:
        return "{}{}".format(self._direction, self._distance)

    def __repl__(self) -> str:
        return self.__str__()
