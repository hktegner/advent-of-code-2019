import os
from Circuit import Circuit
from Point import Point
from SegmentDescriptor import SegmentDescriptor

def find_nearest_intersection(path1: str, path2: str) -> int:
    
    circuit1_descriptors = [SegmentDescriptor(desc) for desc in path1.split(",")]
    circuit2_descriptors = [SegmentDescriptor(desc) for desc in path2.split(",")]

    origin = Point(0, 0)
    circuit1 = Circuit("c1", origin, circuit1_descriptors)
    circuit2 = Circuit("c2", origin, circuit2_descriptors)

    print("{}".format(circuit1))
    print("{}".format(circuit2))

    intersections = Circuit.find_intersections(circuit1, circuit2)
    if (len(intersections) > 0 and intersections[0].x() == 0 and intersections[0].y() == 0):
        intersections = intersections[1:] # ignore origin
    distances = list(map(lambda p: p.manhattan_distance() , intersections))

    print("Found intersections => {}".format(intersections))

    # If no intersections found, return an invalid distance to signal that.
    if (len(distances) == 0):
        return -1

    min_distance = min(distances)

    print("Distance to nearest intersection = {}".format(min_distance))

    return min_distance
