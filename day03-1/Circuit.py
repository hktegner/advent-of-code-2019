from Segment import Segment
from SegmentDescriptor import SegmentDescriptor
from Point import Point


class Circuit:

    def __init__(self, name: str, origin: Point, segment_descriptors: [SegmentDescriptor]):
        self._name = name
        self._segments = []
        
        pointer = origin.copy()
        for descriptor in segment_descriptors:
            segment = Segment(pointer, descriptor)
            pointer = segment.end()
            self._segments.append(segment)

    def name(self) -> str:
        return self._name

    def segments(self) -> [Segment]:
        return self._segments

    def __str__(self) -> str:
        output = "Circuit({}) =\n".format(self._name)
        for segment in self._segments:
            output += "{}\n".format(segment)
        return output

    def __repl__(self) -> str:
        return self.__repl__()

    @classmethod
    def find_intersections(cls, c1: 'Circuit', c2: 'Circuit') -> [Point]:
        intersections = []
        for s1 in c1.segments():
            for s2 in c2.segments():
                intersection = s1.intersection(s2)
                if (intersection is not None):
                    print("Found intersection at", intersection)
                    intersections.append(intersection)
        return intersections