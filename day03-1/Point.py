from SegmentDescriptor import SegmentDescriptor

class Point:

    def __init__(self, x: int, y: int):
        self._x = x
        self._y = y

    def x(self) -> int:
        return self._x

    def y(self) -> int:
        return self._y

    def copy(self):
        return Point(self._x, self._y)

    def move(self, descriptor: SegmentDescriptor):
        direction = descriptor.direction()
        distance = descriptor.distance()
        if (direction == 'U'):
            self._y += distance
        elif (direction == 'D'):
            self._y -= distance
        elif (direction == 'L'):
            self._x -= distance
        elif (direction == 'R'):
            self._x += distance
        
    def __repr__(self) -> str:
        return self.__str__()

    def __str__(self) -> str:
        return "(x={}, y={}, mh={})".format(self._x, self._y, self.manhattan_distance())

    def manhattan_distance(self) -> int:
        return abs(self._x) + abs(self._y)
