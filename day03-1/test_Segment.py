import pytest
from typing import Union
from Segment import Segment
from Point import Point
from SegmentDescriptor import SegmentDescriptor

@pytest.mark.parametrize(
"segment_descriptor,expected_horizontal",
[
    ("U5", False),
    ("D5", False),
    ("R2", True),
    ("L6", True)
])
def test_horizontal_vertical(segment_descriptor: str, expected_horizontal: bool):
    origin = Point(0,0)
    segment = Segment(origin, SegmentDescriptor(segment_descriptor))

    assert segment.horizontal() == expected_horizontal


@pytest.mark.parametrize(
"origin1,descriptor1,origin2,descriptor2,expected_intersection_point",
[
    (Point(0,0), "U5", Point(1, 0), "U5", None),
    (Point(5,0), "U5", Point(2, 2), "R5", Point(5,2)),
    (Point(15,5), "L5", Point(10, 10), "D5", Point(10,5)),
])
def test_segment_creation(
    origin1: Point, descriptor1: str,
    origin2: Point, descriptor2: str,
    expected_intersection_point: Union[None, Point]
    ):
    segment1 = Segment(origin1, SegmentDescriptor(descriptor1))
    segment2 = Segment(origin2, SegmentDescriptor(descriptor2))
    intersection_point = segment1.intersection(segment2)

    if expected_intersection_point is None:
        assert intersection_point is None
    else:
        assert intersection_point is not None
        assert intersection_point.x() == expected_intersection_point.x()
        assert intersection_point.y() == expected_intersection_point.y()

