from math import floor

def calc_fuel(mass: int):
    if (mass <= 0):
        return 0
    fuel = floor(mass / 3) - 2
    if (fuel > 0):
        return fuel + calc_fuel(fuel)
    else:
        return 0


test_data = { 12: 2, 14: 2, 1969: 966, 100756: 50346 }

for mass in test_data.keys():
    calculated_fuel = calc_fuel(mass)
    correct_fuel = test_data[mass]
    print("mass={mass}, calc fuel={calculated_fuel}, correct fuel={correct_fuel}".format(**locals()))

fh = open("module_mass.lst")
total_mass = 0
total_fuel = 0
for line in fh:
    mass = int(line)
    fuel = calc_fuel(mass)
    total_mass += mass
    total_fuel += calc_fuel(mass)

print("total_mass={total_mass}, total_fuel={total_fuel}".format(**locals()))
