
def execute_program(program):
    current = 0
    while True:
        ops_code = program[current]
        if ops_code == 99:
            break
        elif ops_code == 1:
            a = program[program[current+1]]
            b = program[program[current+2]]
            program[program[current+3]] = a+b
        elif ops_code == 2:
            a = program[program[current+1]]
            b = program[program[current+2]]
            program[program[current+3]] = a*b
        current += 4
    return program
    

test_data = [
    dict(prog = [1,0,0,0,99], result = [2,0,0,0,99]),
    dict(prog = [2,3,0,3,99], result = [2,3,0,6,99]),
    dict(prog = [2,4,4,5,99,0], result = [2,4,4,5,99,9801])
]

for test in test_data:
    program = test['prog']
    actual_result = execute_program(program)
    expected_result = test['result']
    print("program={program}, actual_result={actual_result}, expected_result={expected_result}".format(**locals()))

fh = open('1202.txt', mode='r')
raw_program = fh.read()
fh.close()

program = [int(x) for x in raw_program.split(',')]
result = execute_program(program)
print("program={program}, result={result}".format(**locals()))
