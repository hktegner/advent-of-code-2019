import os

def number_of_passwords(from_value: int, to_value: int) -> int:
    valid_pws = 0
    for pw_value in range(from_value, to_value+1):
        if (valid_pw(str(pw_value))):
            valid_pws += 1
    return valid_pws

def valid_pw(pw: str) -> bool:
    if len(pw) < 2:
        return 0

    numbers = [int(char) for char in pw]
    # count events
    decreases = 0
    increases = 0
    doubles = 0
    prev = numbers[0]
    for num in numbers[1:]:
        if num == prev:
            doubles += 1
        if num < prev:
            decreases += 1
        if num > prev:
            increases += 1
        prev = num

    # numbers must stay the same or increase from left to right
    if decreases > 0:
        return False
    
    # must be one double at least
    if doubles < 1:
        return False

    return True
    