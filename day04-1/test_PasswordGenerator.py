import pytest

from PasswordGenerator import number_of_passwords

@pytest.mark.parametrize(
"from_value,to_value,expected_valid_passwords",
[
    (10, 11, 1),
    (10, 12, 1),
    (10, 22, 2),
    (10, 33, 3),
    (10, 100, 9),
    (193651, 649729, 1605),
])
def test_num_passwords(from_value: int, to_value: int, expected_valid_passwords: int):
    assert number_of_passwords(from_value, to_value) == expected_valid_passwords
